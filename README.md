# sjournal

![GitHub Logo](images/screenshot.png)

## Overview

sjournal is a command line based journal / diary application written in bash that stores entries using pgp encryption. 

This project is a work in progress.

## Supported Platforms

* OSX
* Linux

## Dependencies

* gpg
* tree
* sed
* cut
* wc
* srm or shred (platform dependent)

## Contributors

Joe Searle (_joe@joe-searle.com_) [Public Key](http://key.joe-searle.com)

New contributors welcome.
