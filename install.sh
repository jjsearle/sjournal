#!/bin/bash

install -d $HOME/.sjournal
install -d $HOME/.sjournal/templates
install -C templates/default.tpl $HOME/.sjournal/templates
install -C templates/default_markdown.tpl $HOME/.sjournal/templates
install -C bin/sjournal /usr/local/bin/
